# README #

Workflow:
https://trello.com/b/5zjgpq9M/wallapopjobtest

### Consider that... ###

* On a normal workflow, we do Merge Requests in order to do a Code Review. This step is ignored in this project.

* On a normal situation, I would delete all merged branches (except those that are releases)

### Plugins used ###

* Dagger2
* Retrofit2
* Room
* Picasso
* Navigation (safeArgs)
* Timber
* JUnit & Mockito
* LifeCycle
* Androidx

### Thanks to... ###

* https://devexperto.com/clean-architecture-android/
* https://github.com/AweiLoveAndroid/android-architecture-components/blob/master/BasicSample/app/src/main/java/com/example/android/persistence/AppExecutors.java
* https://stackoverflow.com/questions/52829512/using-retrofit-2-for-getting-and-fetching-unknown-set-of-data
* https://gist.github.com/alediaferia/3b5c6042f4c9694274e6e04427123604
* https://medium.com/@hinchman_amanda/the-singlefragmentactivity-pattern-in-android-kotlin-ce93385252e5
* https://github.com/android/architecture-components-samples/tree/master/GithubBrowserSample/app/src/main/java/com/android/example/github/util
* https://github.com/android/architecture-components-samples/blob/master/GithubBrowserSample/app/src/main/java/com/android/example/github/repository/NetworkBoundResource.kt
* https://github.com/android/architecture-components-samples/blob/d81da2cb1e3d61e40f052e631bb15883d0f9f637/GithubBrowserSample/app/src/debug/java/com/android/example/github/testing/OpenForTesting.kt
* https://github.com/android/architecture-components-samples/blob/739993db5a5769afe4ffab0f55622dec6bcd8a22/GithubBrowserSample/app/src/main/java/com/android/example/github/util/AutoClearedValue.kt
