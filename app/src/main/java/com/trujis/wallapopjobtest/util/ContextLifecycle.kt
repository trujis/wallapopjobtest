package com.trujis.wallapopjobtest.util

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner

fun LifecycleOwner.isResumed() = lifecycle.currentState == Lifecycle.State.RESUMED