package com.trujis.wallapopjobtest.util

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData

class CombinedLiveData<M>(vararg sources: LiveData<List<M>>) :
    MediatorLiveData<MutableList<M>>() {

    init {
        sources.forEach { source ->
            super.addSource(source) { list ->
                list?.let {
                    if (value == null)
                        value = mutableListOf()
                    value?.addAll(list)
                }
            }
        }
    }
}