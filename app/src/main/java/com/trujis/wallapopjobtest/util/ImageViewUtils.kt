package com.trujis.wallapopjobtest.util

import android.graphics.drawable.BitmapDrawable
import android.widget.ImageView
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import com.squareup.picasso.Callback
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import timber.log.Timber


fun ImageView.loadCached(uri: String, cornerRadius: Float = 0f) {
    Picasso.get().apply {
        load(uri)
            .networkPolicy(NetworkPolicy.OFFLINE)
            .into(this@loadCached, object : Callback {
                override fun onSuccess() {
                    Timber.d("Image loaded from cache: $uri")
                    this@loadCached.setRoundedCorner(cornerRadius)
                }

                override fun onError(e: Exception) {
                    Timber.d("Image loading from URL: $uri")
                    //Try again online if cache failed
                    Picasso.get().apply {
                        load(uri)
                            .into(this@loadCached, getOnlineCallback(this@loadCached, cornerRadius))
                    }
                }
            })
    }
}

fun ImageView.setRoundedCorner(cornerRadius: Float) {
    if (cornerRadius > 0) {
        this@setRoundedCorner.drawable?.let {
            val imageBitmap = (this@setRoundedCorner.drawable as BitmapDrawable).bitmap
            val imageDrawable =
                RoundedBitmapDrawableFactory.create(this@setRoundedCorner.resources, imageBitmap)
            imageDrawable.isCircular = true
            imageDrawable.cornerRadius = cornerRadius
            this@setRoundedCorner.setImageDrawable(imageDrawable)
        }
    }
}

private fun getOnlineCallback(imageView: ImageView, cornerRadius: Float = 0f): Callback {
    return object : Callback {
        override fun onSuccess() {
            imageView.setRoundedCorner(cornerRadius)
        }

        override fun onError(e: Exception) {
            Timber.e(e, "Picasso. Could not fetch image")
        }
    }
}
