package com.trujis.wallapopjobtest.model

import androidx.room.PrimaryKey
import java.io.Serializable

open class Item(@PrimaryKey var id: String) : Serializable {
    var image: String? = ""
    var price: String? = ""
    var name: String? = ""
    var description: String? = ""
    var distanceInMeters: Int = 0
}