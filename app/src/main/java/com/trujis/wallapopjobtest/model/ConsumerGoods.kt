package com.trujis.wallapopjobtest.model

import androidx.room.Entity
import com.trujis.wallapopjobtest.data.db.ConsumerGoodsDao

@Entity(tableName = ConsumerGoodsDao.TABLE_NAME)
class ConsumerGoods(id: String) : Item(id) {
    var color: String? = ""
    var category: String? = ""
}