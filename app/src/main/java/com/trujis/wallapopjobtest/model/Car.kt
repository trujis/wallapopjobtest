package com.trujis.wallapopjobtest.model

import androidx.room.Entity
import com.trujis.wallapopjobtest.data.db.CarDao

@Entity(tableName = CarDao.TABLE_NAME)
class Car(id: String) : Item(id) {
    var motor: String? = ""
    var gearbox: String? = ""
    var brand: String? = ""
    var km: String? = ""
}