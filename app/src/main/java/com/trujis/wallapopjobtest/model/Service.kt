package com.trujis.wallapopjobtest.model

import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import com.trujis.wallapopjobtest.data.db.ServiceDao

@Entity(tableName = ServiceDao.TABLE_NAME)
class Service(id: String) : Item(id) {
    var closeDay: String? = ""
    var category: String? = ""
    @SerializedName("minimunAge")
    var minimumAge: String? = ""
}