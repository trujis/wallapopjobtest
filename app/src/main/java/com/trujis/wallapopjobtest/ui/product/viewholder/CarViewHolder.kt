package com.trujis.wallapopjobtest.ui.product.viewholder

import com.trujis.wallapopjobtest.databinding.ItemProductCarBinding
import com.trujis.wallapopjobtest.model.Item
import com.trujis.wallapopjobtest.util.loadCached


class CarViewHolder(
    binding: ItemProductCarBinding,
    itemClickCallback: ((Item) -> Unit)?
) :
    ProductItemViewHolder<ItemProductCarBinding>(binding, itemClickCallback) {

    override fun bind(item: Item) {
        binding.item = item
        binding.itemProductImage.loadCached(item.image ?: "", 30f)
    }

    override fun getItem(): Item? = binding.item
}