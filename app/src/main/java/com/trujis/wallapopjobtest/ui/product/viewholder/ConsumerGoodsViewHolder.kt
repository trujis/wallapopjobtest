package com.trujis.wallapopjobtest.ui.product.viewholder

import com.trujis.wallapopjobtest.databinding.ItemProductConsumerGoodsBinding
import com.trujis.wallapopjobtest.model.Item
import com.trujis.wallapopjobtest.util.loadCached

class ConsumerGoodsViewHolder(
    binding: ItemProductConsumerGoodsBinding,
    itemClickCallback: ((Item) -> Unit)?
) :
    ProductItemViewHolder<ItemProductConsumerGoodsBinding>(binding, itemClickCallback) {

    override fun bind(item: Item) {
        binding.item = item
        binding.itemProductImage.loadCached(item.image ?: "", 30f)
    }

    override fun getItem(): Item? = binding.item
}