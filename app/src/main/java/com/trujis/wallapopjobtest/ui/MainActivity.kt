package com.trujis.wallapopjobtest.ui

import android.os.Bundle
import androidx.navigation.findNavController
import com.trujis.wallapopjobtest.R
import com.trujis.wallapopjobtest.ui.common.BaseActivity


class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onBackPressed() {
        if(!findNavController(R.id.container).popBackStack()){
            finish()
        }
    }
}