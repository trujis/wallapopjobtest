package com.trujis.wallapopjobtest.ui.common.binding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.trujis.wallapopjobtest.util.loadCached

object BindingAdapters {
    @JvmStatic
    @BindingAdapter("imageUrl")
    fun setImageUrl(view: ImageView, uri: String) {
        view.loadCached(uri)
    }
}