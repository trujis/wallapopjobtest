package com.trujis.wallapopjobtest.ui.product

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingComponent
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.trujis.wallapopjobtest.R
import com.trujis.wallapopjobtest.data.common.Status
import com.trujis.wallapopjobtest.databinding.FragmentProductListBinding
import com.trujis.wallapopjobtest.model.Car
import com.trujis.wallapopjobtest.model.ConsumerGoods
import com.trujis.wallapopjobtest.persistence.AppExecutors
import com.trujis.wallapopjobtest.ui.common.BaseFragment
import com.trujis.wallapopjobtest.ui.common.ItemGridLayout
import com.trujis.wallapopjobtest.ui.common.binding.FragmentDataBindingComponent
import com.trujis.wallapopjobtest.ui.product.adapter.ProductItemViewHolderFactory
import com.trujis.wallapopjobtest.ui.product.adapter.ProductListAdapter
import com.trujis.wallapopjobtest.util.OpenForTesting
import timber.log.Timber
import javax.inject.Inject

@OpenForTesting
class ProductListFragment :
    BaseFragment<ProductListAdapter, FragmentProductListBinding>(R.layout.fragment_product_list) {

    @Inject
    lateinit var itemGridLayout: ItemGridLayout

    @Inject
    lateinit var appExecutors: AppExecutors

    @Inject
    lateinit var productItemViewHolderFactory: ProductItemViewHolderFactory

    private val productListViewModel: ProductListViewModel by viewModels {
        viewModelFactory
    }

    private var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureBinding()
        configureViewModel()
    }

    private fun configureBinding() {
        val adapter = ProductListAdapter(
            dataBindingComponent,
            productItemViewHolderFactory,
            appExecutors
        ) { menuItem ->
            val action: NavDirections = when (menuItem) {
                is ConsumerGoods -> ProductListFragmentDirections
                    .actionProductListFragment2ToProductDetailFragment(menuItem)
                is Car -> ProductListFragmentDirections
                    .actionProductListFragment2ToProductCarFragment(menuItem)
                else -> ProductListFragmentDirections
                    .actionProductListFragment2ToProductDetailServiceFragment(menuItem)
            }
            findNavController().navigate(action)
        }
        binding.itemList.adapter = adapter
        this.adapter = adapter
        binding.itemList.layoutManager = itemGridLayout.build(requireContext())
    }

    private fun configureViewModel() {
        productListViewModel.getItems().observe(viewLifecycleOwner, Observer { resource ->
            when (resource.status) {
                Status.SUCCESS -> {
                    Timber.d("Loaded ${resource.data?.size} items")
                    this.adapter.submitList(resource.data)
                }
            }
        })
        productListViewModel.loadItems(viewLifecycleOwner)
    }

}