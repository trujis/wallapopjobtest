package com.trujis.wallapopjobtest.ui.common

import android.content.Context
import androidx.recyclerview.widget.GridLayoutManager
import com.trujis.wallapopjobtest.R
import com.trujis.wallapopjobtest.util.OpenForTesting
import javax.inject.Inject

@OpenForTesting
class ItemGridLayout @Inject constructor() {

    fun build(context: Context): GridLayoutManager {
        val numElements = getNumberItems(context)
        return GridLayoutManager(context, numElements)
    }

    protected fun getNumberItems(context: Context): Int {
        val columnWidthDp = context.resources.getDimension(R.dimen.product_list_parent_layout_min_width)
        val displayMetrics = context.resources.displayMetrics
        return kotlin.math.floor(displayMetrics.widthPixels / columnWidthDp).toInt()
    }

}