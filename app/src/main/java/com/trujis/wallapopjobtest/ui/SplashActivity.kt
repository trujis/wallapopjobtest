package com.trujis.wallapopjobtest.ui

import android.content.Intent
import android.os.Bundle
import com.trujis.wallapopjobtest.R
import com.trujis.wallapopjobtest.persistence.AppExecutors
import com.trujis.wallapopjobtest.ui.common.BaseActivity
import com.trujis.wallapopjobtest.ui.product.ProductListViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject


class SplashActivity : BaseActivity() {

    @Inject
    lateinit var productListViewModel: ProductListViewModel

    @Inject
    lateinit var appExecutors: AppExecutors

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        navigateToMainActivity()
        configureViewModel()
    }

    private fun configureViewModel() {
        productListViewModel.loadItems(lifecycleOwner = this)
    }

    private fun navigateToMainActivity() {
        GlobalScope.launch(Dispatchers.Main) {
            delay(SPLASH_TIME)
            val intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    companion object {
        private const val SPLASH_TIME = 2000L
    }

}