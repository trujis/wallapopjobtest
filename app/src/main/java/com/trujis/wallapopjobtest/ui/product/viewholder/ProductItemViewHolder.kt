package com.trujis.wallapopjobtest.ui.product.viewholder

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.trujis.wallapopjobtest.model.Item

abstract class ProductItemViewHolder<T : ViewDataBinding> constructor(
    val binding: T,
    itemClickCallback: ((Item) -> Unit)?
) : RecyclerView.ViewHolder(binding.root) {

    init {
        binding.root.setOnClickListener {
            getItem()?.let { menuItem ->
                itemClickCallback?.invoke(menuItem)
            }
        }
    }

    abstract fun bind(item: Item)
    abstract fun getItem(): Item?
}
