package com.trujis.wallapopjobtest.ui.product.adapter

import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.trujis.wallapopjobtest.model.Car
import com.trujis.wallapopjobtest.model.ConsumerGoods
import com.trujis.wallapopjobtest.model.Item
import com.trujis.wallapopjobtest.persistence.AppExecutors
import com.trujis.wallapopjobtest.ui.product.viewholder.ProductItemViewHolder

class ProductListAdapter(
    private val dataBindingComponent: DataBindingComponent,
    private val productItemViewHolderFactory: ProductItemViewHolderFactory,
    appExecutors: AppExecutors,
    private val itemClickCallback: ((Item) -> Unit)?
) : ListAdapter<Item, ProductItemViewHolder<ViewDataBinding>>(
    AsyncDifferConfig.Builder(object : DiffUtil.ItemCallback<Item>() {
        override fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean {
            return oldItem.name == newItem.name && oldItem.description == newItem.description
        }
    })
        .setBackgroundThreadExecutor(appExecutors.diskIO())
        .build()
) {

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is ConsumerGoods -> TYPE_CONSUMER_GOODS
            is Car -> TYPE_CAR
            else -> TYPE_SERVICE
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ProductItemViewHolder<ViewDataBinding> {
        return productItemViewHolderFactory.createViewHolder(
            dataBindingComponent,
            parent,
            viewType,
            itemClickCallback
        )
    }

    override fun onBindViewHolder(
        holder: ProductItemViewHolder<ViewDataBinding>,
        position: Int
    ) {
        holder.bind(getItem(position))
        holder.binding.executePendingBindings()
    }

    companion object {
        const val TYPE_CONSUMER_GOODS = 1
        const val TYPE_CAR = 2
        const val TYPE_SERVICE = 3
    }
}