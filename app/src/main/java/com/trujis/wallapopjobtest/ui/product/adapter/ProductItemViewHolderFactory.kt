package com.trujis.wallapopjobtest.ui.product.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.trujis.wallapopjobtest.R
import com.trujis.wallapopjobtest.databinding.ItemProductCarBinding
import com.trujis.wallapopjobtest.databinding.ItemProductConsumerGoodsBinding
import com.trujis.wallapopjobtest.databinding.ItemProductServiceBinding
import com.trujis.wallapopjobtest.model.Item
import com.trujis.wallapopjobtest.ui.product.viewholder.CarViewHolder
import com.trujis.wallapopjobtest.ui.product.viewholder.ConsumerGoodsViewHolder
import com.trujis.wallapopjobtest.ui.product.viewholder.ProductItemViewHolder
import com.trujis.wallapopjobtest.ui.product.viewholder.ServiceViewHolder
import javax.inject.Inject


class ProductItemViewHolderFactory @Inject constructor() {

    private lateinit var dataBindingComponent: DataBindingComponent

    fun createViewHolder(
        dataBindingComponent: DataBindingComponent,
        parent: ViewGroup,
        viewType: Int,
        itemClickCallback: ((Item) -> Unit)?
    ): ProductItemViewHolder<ViewDataBinding> {
        this.dataBindingComponent = dataBindingComponent

        return when (viewType) {
            ProductListAdapter.TYPE_CONSUMER_GOODS -> {
                val binding = createBinding<ItemProductConsumerGoodsBinding>(
                    parent,
                    R.layout.item_product_consumer_goods
                )
                ConsumerGoodsViewHolder(
                    binding,
                    itemClickCallback
                ) as ProductItemViewHolder<ViewDataBinding>
            }
            ProductListAdapter.TYPE_SERVICE -> {
                val binding =
                    createBinding<ItemProductServiceBinding>(parent, R.layout.item_product_service)
                ServiceViewHolder(
                    binding,
                    itemClickCallback
                ) as ProductItemViewHolder<ViewDataBinding>
            }
            else -> {
                val binding =
                    createBinding<ItemProductCarBinding>(parent, R.layout.item_product_car)
                CarViewHolder(
                    binding,
                    itemClickCallback
                ) as ProductItemViewHolder<ViewDataBinding>
            }
        }
    }

    private fun <T : ViewDataBinding> createBinding(parent: ViewGroup, layout: Int): T {
        return DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            layout,
            parent,
            false,
            dataBindingComponent
        )
    }

}