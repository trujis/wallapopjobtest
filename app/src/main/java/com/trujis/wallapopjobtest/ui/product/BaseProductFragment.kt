package com.trujis.wallapopjobtest.ui.product

import android.os.Bundle
import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.viewModels
import com.trujis.wallapopjobtest.model.Item
import com.trujis.wallapopjobtest.persistence.AppExecutors
import com.trujis.wallapopjobtest.ui.common.BaseFragment
import com.trujis.wallapopjobtest.util.OpenForTesting
import javax.inject.Inject

@OpenForTesting
abstract class BaseProductFragment<FragmentBinding : ViewDataBinding>(fragmentId: Int) :
    BaseFragment<Any, FragmentBinding>(fragmentId) {

    @Inject
    lateinit var appExecutors: AppExecutors

    protected val productDetailViewModel: ProductDetailViewModel by viewModels {
        viewModelFactory
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureViewModel()
        configureBinding()
    }

    private fun configureViewModel() {
        productDetailViewModel.updateItem(loadArguments())
    }

    abstract fun loadArguments(): Item

    abstract fun configureBinding()
}