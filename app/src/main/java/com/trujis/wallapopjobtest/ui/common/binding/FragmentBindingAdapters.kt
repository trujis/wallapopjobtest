package com.trujis.wallapopjobtest.ui.common.binding

import androidx.fragment.app.Fragment
import com.trujis.wallapopjobtest.util.OpenForTesting
import javax.inject.Inject

/**
 * Binding adapters that work with a fragment instance.
 */
@OpenForTesting
class FragmentBindingAdapters @Inject constructor(val fragment: Fragment) {

}