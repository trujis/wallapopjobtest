package com.trujis.wallapopjobtest.ui.product.viewholder

import com.trujis.wallapopjobtest.databinding.ItemProductServiceBinding
import com.trujis.wallapopjobtest.model.Item
import com.trujis.wallapopjobtest.util.loadCached

class ServiceViewHolder(
    binding: ItemProductServiceBinding,
    itemClickCallback: ((Item) -> Unit)?
) :
    ProductItemViewHolder<ItemProductServiceBinding>(binding, itemClickCallback) {

    override fun bind(item: Item) {
        binding.item = item
        binding.itemProductImage.loadCached(item.image ?: "", 30f)
    }

    override fun getItem(): Item? = binding.item
}