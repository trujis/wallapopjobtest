package com.trujis.wallapopjobtest.ui.product

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.trujis.wallapopjobtest.R
import com.trujis.wallapopjobtest.databinding.FragmentDetailConsumerGoodsBinding
import com.trujis.wallapopjobtest.persistence.AppExecutors
import com.trujis.wallapopjobtest.ui.common.BaseFragment
import com.trujis.wallapopjobtest.util.OpenForTesting
import javax.inject.Inject

@OpenForTesting
class ProductDetailConsumerGoodsFragment :
    BaseProductFragment<FragmentDetailConsumerGoodsBinding>(R.layout.fragment_detail_consumer_goods) {

    val args: ProductDetailConsumerGoodsFragmentArgs by navArgs()

    override fun loadArguments() = args.item

    override fun configureBinding() {
        binding.viewModel = productDetailViewModel
    }
}