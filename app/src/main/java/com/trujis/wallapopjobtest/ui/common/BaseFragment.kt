package com.trujis.wallapopjobtest.ui.common

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.trujis.wallapopjobtest.util.OpenForTesting
import com.trujis.wallapopjobtest.util.autoCleared
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject


@OpenForTesting
abstract class BaseFragment<AdapterType : Any, FragmentBinding : ViewDataBinding> constructor(
    private val fragmentId: Int
) : Fragment(), HasAndroidInjector {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    var adapter by autoCleared<AdapterType>()
    var binding by autoCleared<FragmentBinding>()

    override fun androidInjector(): AndroidInjector<Any> {
        return androidInjector
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val dataBinding =
            DataBindingUtil.inflate<FragmentBinding>(inflater, fragmentId, container, false)
        binding = dataBinding
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.lifecycleOwner = viewLifecycleOwner
    }

    override fun onAttach(context: Context) {
        injectMembers()
        super.onAttach(context)
    }

    protected fun injectMembers() =
        AndroidSupportInjection.inject(this)
}