package com.trujis.wallapopjobtest.ui.product

import androidx.navigation.fragment.navArgs
import com.trujis.wallapopjobtest.R
import com.trujis.wallapopjobtest.databinding.FragmentDetailCarBinding
import com.trujis.wallapopjobtest.util.OpenForTesting

@OpenForTesting
class ProductDetailCarFragment :
    BaseProductFragment<FragmentDetailCarBinding>(R.layout.fragment_detail_car) {

    val args: ProductDetailCarFragmentArgs by navArgs()

    override fun loadArguments() = args.item

    override fun configureBinding() {
        binding.viewModel = productDetailViewModel
    }
}