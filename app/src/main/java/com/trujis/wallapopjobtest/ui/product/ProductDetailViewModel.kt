package com.trujis.wallapopjobtest.ui.product

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.trujis.wallapopjobtest.R
import com.trujis.wallapopjobtest.model.Item
import com.trujis.wallapopjobtest.util.OpenForTesting
import javax.inject.Inject

@OpenForTesting
class ProductDetailViewModel @Inject constructor(
    context: Context
) : ViewModel() {

    val item = MutableLiveData<Item>()
    val distance = MutableLiveData<String>()
    private var literalDistance: String = context.getString(R.string.product_detail_distance)
    private var literalKilometers: String =
        context.getString(R.string.product_detail_distance_kilometers)
    private var literalMeters: String = context.getString(R.string.product_detail_distance_meters)

    fun updateItem(item: Item) {
        this.item.value = item
        updateDistance()
    }

    fun updateDistance() {
        item.value?.let { item ->
            var number = item.distanceInMeters
            var unit = literalMeters
            if (number > 1000) {
                number /= 1000
                unit = literalKilometers
            }
            distance.value = String.format(literalDistance, number, unit)
        }
    }

}