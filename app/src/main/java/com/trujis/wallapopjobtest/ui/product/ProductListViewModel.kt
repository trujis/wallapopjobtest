package com.trujis.wallapopjobtest.ui.product

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.trujis.wallapopjobtest.data.common.Resource
import com.trujis.wallapopjobtest.data.common.Status
import com.trujis.wallapopjobtest.data.repository.ItemRepository
import com.trujis.wallapopjobtest.model.Item
import com.trujis.wallapopjobtest.util.OpenForTesting
import com.trujis.wallapopjobtest.util.isResumed
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@OpenForTesting
class ProductListViewModel @Inject constructor(
    private val itemRepository: ItemRepository
) : ViewModel() {

    private val items = MutableLiveData<Resource<List<Item>>>()
    protected var sortedItems: List<Item>? = null

    fun getItems(): MutableLiveData<Resource<List<Item>>> {
        return items
    }

    fun loadItems(lifecycleOwner: LifecycleOwner) {
        if (sortedItems != null && sortedItems!!.isNotEmpty()) {
            updateItems()
        } else {
            itemRepository.getProducts().observe(lifecycleOwner, Observer { resource ->
                if (lifecycleOwner.isResumed()) {
                    when (resource.status) {
                        Status.LOADING, Status.ERROR -> items.value = resource
                        Status.SUCCESS -> {
                            sortedItems = resource.data?.sortedBy { m -> m.distanceInMeters }
                            updateItems()
                        }
                    }
                }
            })
        }
    }

    private fun updateItems() {
        this.items.value = Resource.success(sortedItems)
    }

}