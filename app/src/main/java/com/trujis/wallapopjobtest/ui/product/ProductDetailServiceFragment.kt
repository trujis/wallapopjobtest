package com.trujis.wallapopjobtest.ui.product

import androidx.navigation.fragment.navArgs
import com.trujis.wallapopjobtest.R
import com.trujis.wallapopjobtest.databinding.FragmentDetailCarBinding
import com.trujis.wallapopjobtest.databinding.FragmentDetailServiceBinding
import com.trujis.wallapopjobtest.util.OpenForTesting

@OpenForTesting
class ProductDetailServiceFragment :
    BaseProductFragment<FragmentDetailServiceBinding>(R.layout.fragment_detail_service) {

    val args: ProductDetailServiceFragmentArgs by navArgs()

    override fun loadArguments() = args.item

    override fun configureBinding() {
        binding.viewModel = productDetailViewModel
    }
}