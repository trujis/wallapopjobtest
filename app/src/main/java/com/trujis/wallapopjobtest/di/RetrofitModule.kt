package com.trujis.wallapopjobtest.di

import com.google.gson.GsonBuilder
import com.trujis.wallapopjobtest.BuildConfig
import com.trujis.wallapopjobtest.data.api.ProductApi
import com.trujis.wallapopjobtest.data.api.ItemDeserializer
import com.trujis.wallapopjobtest.data.api.adapter.LiveDataCallAdapterFactory
import com.trujis.wallapopjobtest.model.Item
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
class RetrofitModule {

    @Singleton
    @Provides
    fun provideProductApi(): ProductApi {
        return getRetrofitAdapter()
            .create(ProductApi::class.java)
    }

    private fun getRetrofitAdapter(): Retrofit {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
        else HttpLoggingInterceptor.Level.NONE

        val client = OkHttpClient.Builder()
            .callTimeout(BuildConfig.API_TIMEOUT, TimeUnit.MILLISECONDS)
            .addInterceptor(interceptor)
            .build()

        val gson = GsonBuilder()
            .registerTypeAdapter(Item::class.java, ItemDeserializer())
            .create()

        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .client(client)
            .build()
    }
}