package com.trujis.wallapopjobtest.di

import com.trujis.wallapopjobtest.ui.product.ProductDetailCarFragment
import com.trujis.wallapopjobtest.ui.product.ProductDetailConsumerGoodsFragment
import com.trujis.wallapopjobtest.ui.product.ProductDetailServiceFragment
import com.trujis.wallapopjobtest.ui.product.ProductListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector()
    abstract fun contributeProductListFragment(): ProductListFragment

    @ContributesAndroidInjector()
    abstract fun contributeProductDetailConsumerGoodsFragment(): ProductDetailConsumerGoodsFragment

    @ContributesAndroidInjector()
    abstract fun contributeProductDetailCarFragment(): ProductDetailCarFragment

    @ContributesAndroidInjector()
    abstract fun contributeProductDetailServiceFragment(): ProductDetailServiceFragment

}