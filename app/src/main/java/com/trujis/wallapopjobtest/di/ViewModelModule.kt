package com.trujis.wallapopjobtest.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.trujis.wallapopjobtest.ui.product.ProductDetailViewModel
import com.trujis.wallapopjobtest.ui.product.ProductListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ProductListViewModel::class)
    abstract fun bindProductListViewModel(viewModel: ProductListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProductDetailViewModel::class)
    abstract fun bindProductDetailViewModel(viewModel: ProductDetailViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: WallapopViewModelFactory): ViewModelProvider.Factory
}