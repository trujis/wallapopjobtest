package com.trujis.wallapopjobtest.di

import android.app.Application
import androidx.room.Room
import com.trujis.wallapopjobtest.data.db.CarDao
import com.trujis.wallapopjobtest.data.db.ConsumerGoodsDao
import com.trujis.wallapopjobtest.data.db.ServiceDao
import com.trujis.wallapopjobtest.data.db.WallapopDb
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class RoomModule {

    @Singleton
    @Provides
    fun provideDb(app: Application): WallapopDb {
        return Room.databaseBuilder(app, WallapopDb::class.java, "wallapop.db")
            .build()
    }

    @Singleton
    @Provides
    fun provideCarDao(db: WallapopDb): CarDao {
        return db.carDao()
    }

    @Singleton
    @Provides
    fun provideConsumerGoodsDao(db: WallapopDb): ConsumerGoodsDao {
        return db.consumerGoodsDao()
    }

    @Singleton
    @Provides
    fun provideServiceDao(db: WallapopDb): ServiceDao {
        return db.serviceDao()
    }

}