package com.trujis.wallapopjobtest.di

import android.app.Application
import android.content.Context
import com.trujis.wallapopjobtest.ui.product.adapter.ProductItemViewHolderFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class AppModule {

    @Singleton
    @Provides
    fun provideContext(app: Application): Context {
        return app.applicationContext
    }

    @Provides
    fun provideProductItemViewHolderFactory(): ProductItemViewHolderFactory {
        return ProductItemViewHolderFactory()
    }

}