package com.trujis.wallapopjobtest.data.api

import com.google.gson.Gson
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.trujis.wallapopjobtest.model.*
import java.lang.reflect.Type

class ItemDeserializer : JsonDeserializer<Item> {

    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): Item? {
        val jsonObject = json?.asJsonObject
        val jsonKind = jsonObject?.get("kind")
        val jsonItem = jsonObject?.get("item")

        val gson = Gson()
        return jsonItem?.let {
            when (jsonKind?.asString) {
                "consumer_goods" -> gson.fromJson(jsonItem, ConsumerGoods::class.java)
                "car" -> gson.fromJson(jsonItem, Car::class.java)
                "service" -> gson.fromJson(jsonItem, Service::class.java)
                else -> null
            }
        } ?: run {
            null
        }
    }
}