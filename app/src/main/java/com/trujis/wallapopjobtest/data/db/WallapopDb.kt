package com.trujis.wallapopjobtest.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.trujis.wallapopjobtest.model.Car
import com.trujis.wallapopjobtest.model.ConsumerGoods
import com.trujis.wallapopjobtest.model.Service

@Database(
    entities = [Car::class, ConsumerGoods::class, Service::class],
    version = 1,
    exportSchema = false
)
abstract class WallapopDb: RoomDatabase() {

    abstract fun carDao(): CarDao
    abstract fun consumerGoodsDao(): ConsumerGoodsDao
    abstract fun serviceDao(): ServiceDao

}