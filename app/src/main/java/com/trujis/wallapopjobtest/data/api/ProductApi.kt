package com.trujis.wallapopjobtest.data.api

import androidx.lifecycle.LiveData
import com.trujis.wallapopjobtest.model.Item
import retrofit2.http.GET

interface ProductApi {

    @GET("Wallapop/Wallapop-Android-Test-Resources/master/items.json")
    fun getProducts(): LiveData<ApiResponse<List<Item>>>

}