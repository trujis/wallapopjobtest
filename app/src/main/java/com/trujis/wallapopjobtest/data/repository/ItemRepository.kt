package com.trujis.wallapopjobtest.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.trujis.wallapopjobtest.data.api.ProductApi
import com.trujis.wallapopjobtest.data.common.Resource
import com.trujis.wallapopjobtest.data.db.CarDao
import com.trujis.wallapopjobtest.data.db.ConsumerGoodsDao
import com.trujis.wallapopjobtest.data.db.ServiceDao
import com.trujis.wallapopjobtest.data.repository.common.NetworkBoundResource
import com.trujis.wallapopjobtest.model.Car
import com.trujis.wallapopjobtest.model.ConsumerGoods
import com.trujis.wallapopjobtest.model.Item
import com.trujis.wallapopjobtest.model.Service
import com.trujis.wallapopjobtest.persistence.AppExecutors
import com.trujis.wallapopjobtest.util.CombinedLiveData
import com.trujis.wallapopjobtest.util.OpenForTesting
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@OpenForTesting
class ItemRepository @Inject constructor(
    private val appExecutors: AppExecutors,
    private val consumerGoodsDao: ConsumerGoodsDao,
    private val carDao: CarDao,
    private val serviceDao: ServiceDao,
    private val productApi: ProductApi
) {

    fun getProducts(): LiveData<Resource<List<Item>>> {
        return object : NetworkBoundResource<List<Item>, List<Item>>(appExecutors) {
            override fun saveCallResult(item: List<Item>) {
                consumerGoodsDao.insert(item.filterIsInstance<ConsumerGoods>())
                carDao.insert(item.filterIsInstance<Car>())
                serviceDao.insert(item.filterIsInstance<Service>())
            }

            override fun shouldFetch(data: List<Item>?) = true
            override fun loadFromDb(): LiveData<List<Item>>? {
                val response : CombinedLiveData<Item> = CombinedLiveData(
                    carDao.get() as LiveData<List<Item>>,
                    serviceDao.get() as LiveData<List<Item>>,
                    consumerGoodsDao.get() as LiveData<List<Item>>
                )
                return response as LiveData<List<Item>>
                //return carDao.get() as LiveData<List<Item>>
            }

            override fun createCall() = productApi.getProducts()
        }.asLiveData()
    }
}