package com.trujis.wallapopjobtest.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Entity
import androidx.room.Query
import com.trujis.wallapopjobtest.data.db.common.BaseDao
import com.trujis.wallapopjobtest.model.Car
import com.trujis.wallapopjobtest.util.OpenForTesting

@Dao
@OpenForTesting
interface CarDao : BaseDao<Car> {

    @Query("SELECT * FROM $TABLE_NAME")
    fun get(): LiveData<List<Car>>

    @Query("DELETE FROM $TABLE_NAME")
    fun deleteAll()

    companion object {
        internal const val TABLE_NAME = "Car"
    }
}