package com.trujis.wallapopjobtest.data.db.common

import androidx.lifecycle.LiveData
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy

interface BaseDao<Input> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(data: Input)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(data: List<Input>)

    @Delete
    fun delete(data: Input)

}
