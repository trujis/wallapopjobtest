package com.trujis.wallapopjobtest.utils

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

object ViewModelUtil {
    fun <T : ViewModel> createFor(vararg model: T): ViewModelProvider.Factory {
        return object : ViewModelProvider.Factory {

            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                model.forEach {model ->
                    if (modelClass.isAssignableFrom(model.javaClass)) {
                        @Suppress("UNCHECKED_CAST")
                        return model as T
                    }
                }
                throw IllegalArgumentException("unexpected model class $modelClass")
            }
        }
    }
}