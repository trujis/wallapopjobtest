package com.trujis.wallapopjobtest.ui.product

import androidx.lifecycle.MutableLiveData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.trujis.wallapopjobtest.R
import com.trujis.wallapopjobtest.data.common.Resource
import com.trujis.wallapopjobtest.model.Item
import com.trujis.wallapopjobtest.testing.SingleFragmentActivity
import com.trujis.wallapopjobtest.ui.common.ItemGridLayout
import com.trujis.wallapopjobtest.utils.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations


@RunWith(AndroidJUnit4::class)
class ProductListFragmentTest {

    @Rule
    @JvmField
    val activityRule = ActivityTestRule(SingleFragmentActivity::class.java, true, true)

    @Rule
    @JvmField
    val countingAppExecutors = CountingAppExecutorsRule()

    @Rule
    @JvmField
    val dataBindingIdlingResourceRule = DataBindingIdlingResourceRule(activityRule)


    @Mock
    private lateinit var productListViewModelMock: ProductListViewModel

    private val itemsLiveData = MutableLiveData<Resource<List<Item>>>()
    private val testFragment = TestFragment()


    @Before
    fun init() {
        MockitoAnnotations.initMocks(this)
        testFragment.appExecutors = countingAppExecutors.appExecutors
        testFragment.viewModelFactory = ViewModelUtil.createFor(productListViewModelMock)

        Mockito.`when`(productListViewModelMock.getItems()).thenReturn(itemsLiveData)
        activityRule.activity.replaceFragment(testFragment)
    }

    @Test
    fun shouldNotHaveAnyItemOnRecyclerViewWhenNoProductsAreAdded() {
        //Assert
        onView(withId(R.id.item_list)).check(RecyclerViewItemCountAssertion.withItemCount(0))
    }
/*
    @Test
    fun shouldShowItemsOnScreen() {
        //Arrange
        val items = mutableListOf<Item>()
        repeat(5) {
            val car = Car("id1")
            car.name = "Name Car"
            car.description = "Description car"
            items.add(car)
        }

        //Act
        itemsLiveData.postValue(Resource.success(items))

        //Assert
        for (index in items.indices) {
            Espresso.onView(RecyclerViewMatcher(R.id.item_list).atPosition(index)).apply {
                check(ViewAssertions.matches(ViewMatchers.hasDescendant(withText(items[index].name))))
            }
        }
    }*/

    companion object {
        @Mock
        lateinit var itemGridLayoutMock: ItemGridLayout
    }

    class TestFragment : ProductListFragment() {
        override fun injectMembers() {
            this.itemGridLayout = itemGridLayoutMock
        }
    }
}