package com.trujis.wallapopjobtest.ui.product

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.trujis.wallapopjobtest.data.common.Resource
import com.trujis.wallapopjobtest.data.common.Status
import com.trujis.wallapopjobtest.data.repository.ItemRepository
import com.trujis.wallapopjobtest.model.Car
import com.trujis.wallapopjobtest.model.Item
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations


@RunWith(AndroidJUnit4::class)
class ProductListViewModelTest {

    @Rule
    @JvmField
    val activityRule = InstantTaskExecutorRule()

    private lateinit var viewModel: TestViewModel

    @Mock
    lateinit var observerMock: Observer<Resource<List<Item>>>

    @Mock
    lateinit var lifecycleOwnerMock: LifecycleOwner

    @Mock
    lateinit var itemRepositoryMock: ItemRepository

    @Mock
    lateinit var lifecycleMock: Lifecycle
    private var itemsLiveData = MutableLiveData<Resource<List<Item>>>()

    @Before
    fun init() {
        MockitoAnnotations.initMocks(this)

        Mockito.`when`(itemRepositoryMock.getProducts()).thenReturn(itemsLiveData)
        Mockito.`when`(lifecycleOwnerMock.lifecycle).thenReturn(lifecycleMock)

        viewModel = TestViewModel(itemRepositoryMock)
        viewModel.getItems().observeForever(observerMock)
    }

    /*
    @Test
    fun shouldPostItemsWhenGettingProductsSuccessfully() {
        //Arrange
        val items = mutableListOf<Item>()
        repeat(5) {
            val car = Car("id$it")
            car.name = "Name Car"
            car.description = "Description car"
            items.add(car)
        }
        Mockito.`when`(lifecycleMock.currentState).thenReturn(Lifecycle.State.RESUMED)
        val arg: ArgumentCaptor<Resource<*>>? = ArgumentCaptor.forClass(Resource::class.java)

        //Act
        viewModel.loadItems(lifecycleOwnerMock)
        itemsLiveData.postValue(Resource.success(items))
        Mockito.verify(observerMock, Mockito.times(1)).onChanged(arg?.capture() as Resource<List<Item>>?)
        val values = arg?.allValues

        //Assert
        //Mockito.verify(observerMock).onChanged(headerSubtitle)
        Assert.assertEquals(1, values?.size)
        values?.let {
            Assert.assertEquals(Status.SUCCESS, values[0].status)
            Assert.assertEquals(5, (values[0].data as List<Item>).size)
            Assert.assertEquals("id1", (values[0].data as List<Item>)[0].id)
            Assert.assertEquals("Name Car", (values[0].data as List<Item>)[0].name)
        }
    }*/

    @Test
    fun shouldNotCallRepositoryWhenLoadingItemsGivenAnAlreadyLoadedItems() {
        //Arrange
        val items = mutableListOf<Item>()
        items.add(Car("id"))
        viewModel.setItems(items)

        //Act
        viewModel.loadItems(lifecycleOwnerMock)

        //Assert
        Mockito.verify(itemRepositoryMock, Mockito.times(0)).getProducts()
    }

    @Test
    fun shouldPostItemsWhenGettingProductsAlreadyLoaded() {
        //Arrange
        val items = mutableListOf<Item>()
        items.add(Car("id"))
        items.add(Car("id2"))
        viewModel.setItems(items)
        val arg: ArgumentCaptor<Resource<*>>? = ArgumentCaptor.forClass(Resource::class.java)

        //Act
        viewModel.loadItems(lifecycleOwnerMock)
        Mockito.verify(observerMock, Mockito.times(1)).onChanged(arg?.capture() as Resource<List<Item>>?)
        val values = arg?.allValues

        //Assert
        Assert.assertEquals(1, values?.size)
        values?.let {
            Assert.assertEquals(Status.SUCCESS, values[0].status)
            Assert.assertEquals(2, (values[0].data as List<Item>).size)
            Assert.assertEquals("id", (values[0].data as List<Item>)[0].id)
            Assert.assertEquals("id2", (values[0].data as List<Item>)[1].id)
        }
    }

    class TestViewModel(itemRepository: ItemRepository) : ProductListViewModel(itemRepository) {
        fun setItems(items : List<Item>?) {
            this.sortedItems = items
        }
    }
}