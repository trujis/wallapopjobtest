package com.trujis.wallapopjobtest.data.db

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.trujis.wallapopjobtest.data.db.common.BaseDao
import com.trujis.wallapopjobtest.model.Item
import com.trujis.wallapopjobtest.utils.observeOnce
import junit.framework.Assert.assertEquals
import org.junit.After
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException


@RunWith(AndroidJUnit4::class)
abstract class BaseDaoTest<DAO: BaseDao<ProductItem>, ProductItem: Item> {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    protected var dao: DAO? = null
    protected lateinit var wallapopDb: WallapopDb

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        wallapopDb = Room.inMemoryDatabaseBuilder(
            context, WallapopDb::class.java
        ).build()
        dao = getDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        wallapopDb.close()
    }


    internal abstract fun getDao() : DAO

    protected fun assertEqualObject(expected: ProductItem, actual: ProductItem?) {
        assertNotNull(actual)
        assertEquals(expected.id, actual!!.id)
        assertEquals(expected.description, actual.description)
        assertEquals(expected.distanceInMeters, actual.distanceInMeters)
        assertEquals(expected.image, actual.image)
        assertEquals(expected.name, actual.name)
        assertEquals(expected.price, actual.price)
        assertEqualSpecificItem(expected, actual)
    }

    internal abstract fun assertEqualSpecificItem(expected: ProductItem, actual: ProductItem)
}