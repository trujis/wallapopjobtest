package com.trujis.wallapopjobtest.data.db

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.trujis.wallapopjobtest.model.Car
import com.trujis.wallapopjobtest.model.ConsumerGoods
import com.trujis.wallapopjobtest.utils.observeOnce
import junit.framework.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class ConsumerGoodsDaoTest : BaseDaoTest<ConsumerGoodsDao, ConsumerGoods>() {

    @Test
    fun shouldReadFromDBAStoredObject() {
        //Arrange
        val item = ConsumerGoods("id")
        item.color = "color"
        item.category = "category"
        item.description = "desc"
        item.distanceInMeters = 1
        item.image = "im"
        item.name = "name"
        item.price = "p"
        dao!!.insert(item)

        //Act
        val dbStored = dao!!.get()
        dbStored.observeOnce {
            //Assert
            assertNotNull(it)
            assertEquals(1, it.size)
            assertEqualObject(item, it[0])
        }
    }

    @Test
    fun shouldReplaceAnObjectWhenInsertingTheSameId() {
        //Arrange
        val items = mutableListOf<ConsumerGoods>()
        repeat(2) {
            val item = ConsumerGoods("id")
            item.color = "color_$it"
            item.category = "category_$it"
            item.description = "desc_$it"
            item.distanceInMeters = it
            item.image = "im_$it"
            item.name = "name_$it"
            item.price = "p_$it"
            dao!!.insert(item)
            items.add(item)
        }

        //Act
        val dbStored = dao!!.get()
        dbStored.observeOnce {
            //Assert
            assertNotNull(it)
            assertEquals(1, it.size)
            assertEqualObject(items.last(), it[0])
        }
    }

    @Test
    fun shouldReadFromDBMultipleStoredObjects() {
        //Arrange
        val items = mutableListOf<ConsumerGoods>()
        repeat(2) {
            items.add(ConsumerGoods("id_$it"))
        }
        dao!!.insert(items)

        //Act
        val dbStored = dao!!.get()
        dbStored.observeOnce {
            //Assert
            assertNotNull(it)
            assertEquals(2, it.size)
            assertEquals("id_0", it[0].id)
            assertEquals("id_1", it[1].id)
        }
    }

    @Test
    fun shouldReadNothingFromDBWhenDeletingStoredObjects() {
        //Arrange
        repeat(2) {
            dao!!.insert(ConsumerGoods("id_$it"))
        }

        //Act
        val dbStored = dao!!.get()
        dbStored.observeOnce {
            //Assert
            assertNotNull(it)
            assertEquals(2, it.size)

            dao!!.deleteAll()
            val dbDeleted = dao!!.get()
            dbDeleted.observeOnce { deleted ->
                //Assert
                assertNotNull(deleted)
                assertEquals(0, deleted.size)
            }
        }
    }

    override fun getDao(): ConsumerGoodsDao {
        return wallapopDb.consumerGoodsDao()
    }

    override fun assertEqualSpecificItem(expected: ConsumerGoods, actual: ConsumerGoods) {
        assertEquals(expected.color, actual.color)
        assertEquals(expected.category, actual.category)
    }
}