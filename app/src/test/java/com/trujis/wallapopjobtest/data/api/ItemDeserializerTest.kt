package com.trujis.wallapopjobtest.data.api

import com.google.gson.Gson
import com.google.gson.JsonElement
import com.trujis.wallapopjobtest.model.Car
import com.trujis.wallapopjobtest.model.ConsumerGoods
import com.trujis.wallapopjobtest.model.Service
import org.hamcrest.core.IsInstanceOf.instanceOf
import org.junit.Assert
import org.junit.Before
import org.junit.Test


class ItemDeserializerTest {

    private lateinit var itemDeserializer: ItemDeserializer
    private lateinit var gson: Gson

    @Before
    fun before() {
        gson = Gson()
        itemDeserializer = ItemDeserializer()
    }

    @Test
    fun `Return nothing when deserializing a non valid item`() {
        //Arrange
        val content = "{\"kind\":\"consumer_bads\",\"item\":{\"id\":\"123456qwert\"}}"
        val jsonItem = gson.fromJson(content, JsonElement::class.java)

        //Act
        val item = itemDeserializer.deserialize(jsonItem, null, null)

        //Assert
        Assert.assertNull(item)
    }

    @Test
    fun `Return nothing when deserializing a null jsonElement`() {
        //Act
        val item = itemDeserializer.deserialize(null, null, null)

        //Assert
        Assert.assertNull(item)
    }

    @Test
    fun `Return nothing when deserializing a json without kind key`() {
        //Arrange
        val content = "{\"item\":{\"id\":\"123456qwert\"}}"
        val jsonItem = gson.fromJson(content, JsonElement::class.java)

        //Act
        val item = itemDeserializer.deserialize(jsonItem, null, null)

        //Assert
        Assert.assertNull(item)
    }

    @Test
    fun `Return nothing when deserializing a json without item key`() {
        //Arrange
        val content = "{\"kind\":\"consumer_goods\"}"
        val jsonItem = gson.fromJson(content, JsonElement::class.java)

        //Act
        val item = itemDeserializer.deserialize(jsonItem, null, null)

        //Assert
        Assert.assertNull(item)
    }

    @Test
    fun `Return a valid consumer_goods item when deserializing a valid item`() {
        //Arrange
        val id = "123456qwert"
        val content = "{\"kind\":\"consumer_goods\",\"item\":{\"id\":\"$id\"}}"
        val jsonItem = gson.fromJson(content, JsonElement::class.java)

        //Act
        val item = itemDeserializer.deserialize(jsonItem, null, null)

        //Assert
        Assert.assertNotNull(item)
        Assert.assertThat(item, instanceOf(ConsumerGoods::class.java))
        Assert.assertEquals(id, item!!.id)
    }

    @Test
    fun `Return a valid car item when deserializing a valid item`() {
        //Arrange
        val id = "123456qwert"
        val content = "{\"kind\":\"car\",\"item\":{\"id\":\"$id\"}}"
        val jsonItem = gson.fromJson(content, JsonElement::class.java)

        //Act
        val item = itemDeserializer.deserialize(jsonItem, null, null)

        //Assert
        Assert.assertNotNull(item)
        Assert.assertThat(item, instanceOf(Car::class.java))
        Assert.assertEquals(id, item!!.id)
    }

    @Test
    fun `Return a valid service item when deserializing a valid item`() {
        //Arrange
        val id = "123456qwert"
        val content = "{\"kind\":\"service\",\"item\":{\"id\":\"$id\"}}"
        val jsonItem = gson.fromJson(content, JsonElement::class.java)

        //Act
        val item = itemDeserializer.deserialize(jsonItem, null, null)

        //Assert
        Assert.assertNotNull(item)
        Assert.assertThat(item, instanceOf(Service::class.java))
        Assert.assertEquals(id, item!!.id)
    }
}