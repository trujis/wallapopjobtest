package com.trujis.wallapopjobtest.ui.common

import android.content.Context
import android.content.res.Resources
import android.util.DisplayMetrics
import com.trujis.wallapopjobtest.R
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
internal class ItemGridLayoutTest {

    @Mock
    private lateinit var context: Context
    @Mock
    private lateinit var resources: Resources
    @Mock
    private lateinit var displayMetrics: DisplayMetrics

    private lateinit var itemGridLayout: ItemGridLayoutImplTest

    @Before
    fun setUp() {
        itemGridLayout = ItemGridLayoutImplTest()
        Mockito.`when`(context.resources).thenReturn(resources)
        Mockito.`when`(resources.displayMetrics).thenReturn(displayMetrics)
    }

    @Test
    fun `Get width dimension When building`() {
        //Arrange

        //Act
        itemGridLayout.testNumberItems(context)

        //Assert
        Mockito.verify(resources, Mockito.times(1)).getDimension(R.dimen.product_list_parent_layout_min_width)
        Mockito.verify(context, Mockito.times(2)).resources
    }

    @Test
    fun `Get 2 items When screen can hold a little more than 2 items`() {
        //Arrange
        Mockito.`when`(resources.getDimension(R.dimen.product_list_parent_layout_min_width)).thenReturn(12f)
        displayMetrics.widthPixels = 25

        //Act
        val numberItems = itemGridLayout.testNumberItems(context)

        //Assert
        Assert.assertEquals(2, numberItems)
    }

    @Test
    fun `Get 1 items When screen can hold almost 2 items`() {
        //Arrange
        Mockito.`when`(resources.getDimension(R.dimen.product_list_parent_layout_min_width)).thenReturn(13f)
        displayMetrics.widthPixels = 25

        //Act
        val numberItems = itemGridLayout.testNumberItems(context)

        //Assert
        Assert.assertEquals(1, numberItems)
    }

    class ItemGridLayoutImplTest: ItemGridLayout() {
        fun testNumberItems(context: Context): Int {
            return this.getNumberItems(context)
        }
    }
}
